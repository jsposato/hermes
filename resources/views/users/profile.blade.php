@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2" >

                {!! Form::open([
                        'route' => 'user.update',
                        'method' => 'post',
                        'class' => 'form-horizontal'
                    ]) !!}
                <div class="form-group">
                    {!!
                        Form::label('name', 'Name',
                        [
                            'for'   => 'name',
                            'class' => 'control-label'
                        ])
                    !!}
                    {!! Form::text('Name', $currentUser->name,
                        [
                            'class' => 'form-control',
                            'id'    => 'name'
                        ])
                    !!}
                </div>
                <div class="form-group">
                    {!!
                        Form::label('email', 'Email',
                        [
                            'for'   => 'email',
                            'class' => 'control-label'
                        ])
                    !!}
                    {!! Form::text('Email', $currentUser->email,
                        [
                            'class' => 'form-control',
                            'id'    => 'email'
                        ])
                    !!}
                </div>
                <div class="form-group">
                    {!!
                        Form::label('jiraUrl', 'JIRA URL',
                        [
                            'for'   => 'jiraUrl',
                            'class' => 'control-label'
                        ])
                    !!}
                    {!! Form::text('JIRA URL', $currentUser->jira_url,
                        [
                            'class' => 'form-control',
                            'id'    => 'jiraUrl'
                        ])
                    !!}
                </div>
                <div class="form-group">
                    {!!
                        Form::label('jiraUser', 'JIRA Username',
                        [
                            'for'   => 'jiraUser',
                            'class' => 'control-label'
                        ])
                    !!}
                    {!! Form::text('JIRA Username', $currentUser->jira_username,
                        [
                            'class' => 'form-control',
                            'id'    => 'jiraUser'
                        ])
                    !!}
                </div>
                <div class="form-group">
                    {!!
                        Form::label('jiraPassword', 'JIRA Password',
                        [
                            'for'   => 'jiraPassword',
                            'class' => 'control-label'
                        ])
                    !!}
                    {!! Form::password('JIRA Password',
                        [
                            'class' => 'form-control',
                            'id'    => 'jiraPassword'
                        ])
                    !!}
                </div>

                {!! Form::submit('Submit', [
                    'class' => 'btn btn-default'
                ]) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
