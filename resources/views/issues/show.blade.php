@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                Download Issue Template {!! Html::link("files/data.csv","Here") !!}<br /><br />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2" >
                {!! Form::open([
                        'route' => 'issues.import',
                        'method' => 'post',
                        'files' => true,
                        'class' => 'form-inline'
                    ]) !!}

                {!! Form::file('File', [
                    'class' => 'form-group'
                ]) !!}

                {!! Form::submit('Submit', [
                    'class' => 'btn btn-default'
                ]) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
