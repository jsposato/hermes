<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/home');
})->name('home');

// Health check route for ELB
Route::get('/healthcheck', function () {
    return response()->json('Healthy', 200);
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('issues/import', 'IssuesController@show')->name('issues.show');
    Route::post('issues/import', 'IssuesController@import')->name('issues.import');
    Route::get('user/{id}', 'UsersController@showProfile')->name('user.profile');
    Route::post('user', 'UsersController@updateProfile')->name('user.update');
});

