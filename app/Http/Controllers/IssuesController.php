<?php

namespace Hermes\Http\Controllers;

use Aws\Sqs\SqsClient;
use GuzzleHttp\Client;
use Hermes\Http\Requests;

class IssuesController extends Controller
{
    /**
     * show
     *
     * show form to upload data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {

        return view('issues.show');
    }

    /**
     * import
     *
     * import CSV to JIRA
     *
     * TODO: Import to SQS, write a listener to process SQS
     *
     */
    public function import()
    {

        try {
            $handle = fopen($_FILES['File']['tmp_name'], 'r');
        } catch (Exception $e) {
            \Log::critical($e->getMessage());
        }

        $sqs = SqsClient::factory([
            'key'     => env('AWS_ACCESS_KEY_ID'),
            'secret'  => env('AWS_SECRET_ACCESS_KEY'),
            'region'  => 'us-east-1',
            'version' => 'latest'
        ]);

        while ($line = fgetcsv($handle)) {

            if ($line[0] == "Project") {
                continue;
            }

            $project     = $line[0];
            $summary     = $line[1];
            $description = $line[2];
            $issueType   = $line[3];
            $components  = $this->formatComponents($line[4]);

            $data = [
                "fields" => [
                    "project" => [
                        "key" => $project
                    ],
                    "summary" => $summary,
                    "description" => $description,
                    "issuetype" => [
                        "name" => $issueType
                    ],
                    "components" => $components
                ]
            ];
            $data = json_encode($data);

            $sqs->sendMessage([
                'QueueUrl'    => env('AWS_SQS_QUEUE_URL'),
                'MessageBody' => $data,
                'MessageAttributes' => [
                    'userId' => [
                        'StringValue' => '1',
                        'DataType' => 'Number'
                    ]
                ]
            ]);
        }
    }

    /**
     * formatComponents
     *
     * take a comma separated list of components and return a proper array for JIRA request
     *
     * TODO: make general for any array of arrays needed
     *
     * @param $components
     * @return array
     */
    private function formatComponents($components)
    {
        $tmpArray = explode(",", $components);

        $retVal = array();

        foreach ($tmpArray as $component) {
            $retVal[]['name'] = $component;
        }

        return $retVal;
    }
}
