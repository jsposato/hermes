<?php

namespace Hermes\Http\Controllers;

use Hermes\Http\Requests;
use Hermes\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * showProfile
     *
     * show the profile for the passed user
     *
     * @param $id
     * @param User $user
     * @return $this
     */
    public function showProfile($id, User $user, Request $request)
    {
        if ($id != \Auth::user()->id) {
            $request->session()->flash('error', 'You are not authorized to view that profile');

            return redirect('home');
        }
        $currentUser = $user->findOrFail($id);

        return view('users.profile')->with(['currentUser' => $currentUser]);
    }

    /**
     * updateProfile
     *
     * update the user from the request
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateProfile(Request $request)
    {
        $user = User::find($request->user()->id);

        $user->name          = $request->get('Name');
        $user->email         = $request->get('Email');
        $user->jira_url      = $request->get('JIRA_URL');
        $user->jira_username = $request->get('JIRA_Username');

        if(!$user->save()) {
            $request->session()->flash('error', 'Unable to update profile');
        }
        
        $request->session()->flash('info', 'Profile updated!');

        $redirectPath = '/user/' . $request->user()->id;
        return redirect($redirectPath);

    }
}
