<?php

namespace Hermes\Console\Commands;

use Aws\Sqs\SqsClient;
use GuzzleHttp\Client;
use Hermes\Models\User;
use Illuminate\Console\Command;

class ProcessQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hermes:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process items in the SQS queue';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sqs = SqsClient::factory([
            'key'     => env('AWS_ACCESS_KEY_ID'),
            'secret'  => env('AWS_SECRET_ACCESS_KEY'),
            'region'  => 'us-east-1',
            'version' => 'latest'
        ]);

        $result = $sqs->receiveMessage([
            'QueueUrl' => env('AWS_SQS_QUEUE_URL'),
            'MaxNumberOfMessages' => 10,
            'MessageAttributeNames' => ['All'],
        ]);

        if(count($result->getPath('Messages')) > 0) {
            foreach ($result->getPath('Messages') as $message) {
                // Do something with the message
                $this->sendToJira($message['Body'], $message['MessageAttributes']['userId']['StringValue']);

                // Delete the message
                $result = $sqs->deleteMessage([
                    // QueueUrl is required
                    'QueueUrl' => env('AWS_SQS_QUEUE_URL'),
                    // ReceiptHandle is required
                    'ReceiptHandle' => $message['ReceiptHandle'],
                ]);
            }
        } else {
            $date = date('Y-m-d', strtotime(date('m/d/Y H:m')));
            \Log::info($date." - No messages in the queue");
        }
    }

    /**
     * sendToJira
     *
     * send request to JIRA for processing
     *
     * @param $data
     * @param $userId
     */
    private function sendToJira($data, $userId) {

        $user = User::find($userId);

        $client = new Client([
            'base_uri' => $user->jira_url,
            'timeout' => 2.0,
        ]);

        $response = $client->post("issue", [
            'auth' => [$user->jira_username, $user->jira_password],
            'body' => $data,
            'headers' => [
                "Content-Type" => "application/json"
            ]
        ]);

        \Log::info('Status from JIRA: '.$response->getStatusCode());
        \Log::info('Data: '.$data);

    }
}
