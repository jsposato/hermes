<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJiraInfoToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('jira_url', 250)->after('remember_token');
            $table->string('jira_username', 100)->after('jira_url');
            $table->string('jira_password', 100)->after('jira_username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn(['jira_url', 'jira_username', 'jira_password']);
        });
    }
}
